package com.cateringapp.exception;

import com.cateringapp.exception.korisnik.KorisnikErrorResponse;
import com.cateringapp.exception.korisnik.KorisnikNotFoundException;
import com.cateringapp.exception.meal.MealErrorResponse;
import com.cateringapp.exception.meal.MealNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class RestExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<KorisnikErrorResponse> handleException(KorisnikNotFoundException exc) {

        KorisnikErrorResponse error = new KorisnikErrorResponse(HttpStatus.NOT_FOUND.value(),
                exc.getMessage(),
                System.currentTimeMillis());

        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    public ResponseEntity<KorisnikErrorResponse> handleException(Exception exc) {

        KorisnikErrorResponse error = new KorisnikErrorResponse(HttpStatus.BAD_REQUEST.value(),
                exc.getMessage(),
                System.currentTimeMillis());

        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public ResponseEntity<MealErrorResponse> handleException(MealNotFoundException exc) {

        MealErrorResponse error = new MealErrorResponse(HttpStatus.NOT_FOUND.value(),
                exc.getMessage(),
                System.currentTimeMillis());
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

//    @ExceptionHandler
//    public ResponseEntity<MealErrorResponse> handleException(Exception exc) {
//
//        MealErrorResponse error = new MealErrorResponse(HttpStatus.BAD_REQUEST.value(),
//                exc.getMessage(),
//                System.currentTimeMillis());
//        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
//    }
}
