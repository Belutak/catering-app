package com.cateringapp.exception.korisnik;

public class KorisnikNotFoundException extends Exception{

    public KorisnikNotFoundException() {
    }

    public KorisnikNotFoundException(String message) {
        super(message);
    }

    public KorisnikNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public KorisnikNotFoundException(Throwable cause) {
        super(cause);
    }

    public KorisnikNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
