package com.cateringapp.service.impl;

import com.cateringapp.exception.korisnik.KorisnikNotFoundException;
import com.cateringapp.model.Meal;
import com.cateringapp.repository.KorisnikRepository;
import com.cateringapp.model.Korisnik;
import com.cateringapp.repository.MealRepository;
import com.cateringapp.rest.dto.korisnikDTO.KorisnikDTO;
import com.cateringapp.rest.dto.korisnikDTO.KorisnikResponse;
import com.cateringapp.service.KorisnikService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class KorisnikServiceImpl implements KorisnikService {

    @Autowired
    private KorisnikRepository korisnikRepo;

    @Autowired
    MealRepository mealRepo;

    @Override
    public KorisnikDTO saveNew(KorisnikDTO korisnikDTO) {

        Korisnik tempKorisnik = new Korisnik(korisnikDTO.getFirstName(), korisnikDTO.getLastName(), korisnikDTO.getEmail());

        korisnikRepo.save(tempKorisnik);
        return korisnikDTO;
    }

    @Override
    public KorisnikDTO getByName(String firstname) {

        Korisnik tempKorisnik = korisnikRepo.findByFirstName(firstname);

        return new KorisnikDTO(tempKorisnik.getFirstName(), tempKorisnik.getLastName(), tempKorisnik.getEmail());
    }

    @Override
    public List<KorisnikDTO> getAllNew() {

        List<Korisnik> listKorisnik = (List<Korisnik>) korisnikRepo.findAll();
        List<KorisnikDTO> listKorisnikDTO = new ArrayList<>();

        for (Korisnik temp : listKorisnik) {

            listKorisnikDTO.add(new KorisnikDTO(temp.getFirstName(), temp.getLastName(), temp.getEmail()));
        }

        return listKorisnikDTO;
    }

    @Override
    public KorisnikDTO updateKorisnikNew(KorisnikDTO korisnikDTO, long id) throws KorisnikNotFoundException {

        Korisnik tempKorisnik = korisnikRepo.findById(id).orElse(null);

        if (tempKorisnik == null || korisnikDTO == null) {
            throw new KorisnikNotFoundException("Korisnik with id: " + id + " - not found.");
        }

        tempKorisnik.setEmail(korisnikDTO.getEmail());
        tempKorisnik.setLastName(korisnikDTO.getLastName());
        tempKorisnik.setFirstName(korisnikDTO.getFirstName());

        return korisnikDTO;
    }

    @Override
    public void delete(String firstName) {

        Korisnik korisnik = korisnikRepo.findByFirstName(firstName);
        korisnikRepo.deleteById(korisnik.getId());
    }


    @Override
    public KorisnikResponse returnMealsT(String firstname) {

        Korisnik tempKorisnik = korisnikRepo.findByFirstName(firstname);
        Meal tempMeal1 = mealRepo.findByMealName("Taco");
        Meal tempMeal2 = mealRepo.findByMealName("Fries");

        KorisnikResponse korisnikResponse = new KorisnikResponse(tempKorisnik.getFirstName(), tempKorisnik.getLastName(), tempKorisnik.getEmail(), tempKorisnik.getListMeals());
        tempKorisnik.addMealToKorisnik(tempMeal1);
        tempKorisnik.addMealToKorisnik(tempMeal2);

        return korisnikResponse;
    }

//    @Override
//    public void create(Korisnik korisnik) {
//
//        korisnikRepo.save(korisnik);
//    }
//    public List<Korisnik> getAll() {
//
//        List<Korisnik> listKorisnik = new ArrayList<>();
//
//        return listKorisnik = (List<Korisnik>) korisnikRepo.findAll();
//    }
//
//    public Korisnik getOne(long id) {
//
//        Korisnik korisnikOne = korisnikRepo.findById(id).get();
//
//        return korisnikOne;
//    }
//
//    @Override
//    public Korisnik updateKorisnik(Korisnik korisnik, long id) {
//
//        Korisnik k1 = korisnikRepo.findById(id).get();
//
//        if (k1 == null || korisnik == null) {
//            throw new NullPointerException();
//        }
//
//        k1.setEmail(korisnik.getEmail());
//        k1.setLastName(korisnik.getLastName());
//        k1.setFirstName(korisnik.getFirstName());
//
//        return k1;
//    }
//
//    @Override
//    public void deleteWithId(long id) {
//
//        korisnikRepo.deleteById(id);
//    }

}
