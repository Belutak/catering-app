package com.cateringapp.service.impl;

import com.cateringapp.exception.meal.MealNotFoundException;

import com.cateringapp.model.Korisnik;
import com.cateringapp.model.KorisnikMeals;
import com.cateringapp.model.Meal;
import com.cateringapp.repository.MealRepository;
import com.cateringapp.rest.dto.mealDTO.MealDTO;
import com.cateringapp.rest.dto.mealDTO.MealResponse;
import com.cateringapp.service.MealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class MealServiceImpl implements MealService {

    @Autowired
    private MealRepository mealRepo;


    @Override
    public MealDTO saveNew(MealDTO mealDTO) {

        Meal tempMeal = new Meal(mealDTO.getMealName());
        mealRepo.save(tempMeal);
        return mealDTO;
    }

    @Override
    public MealDTO getByName(String mealName) {

        Meal tempMeal = mealRepo.findByMealName(mealName);

        return new MealDTO(tempMeal.getMealName());
    }

    @Override
    public List<MealDTO> getAll() {

        List<Meal> listMeal = (List<Meal>) mealRepo.findAll();
        List<MealDTO> listMealDTO = new ArrayList<>();

        for (Meal temp : listMeal) {

            listMealDTO.add(new MealDTO(temp.getMealName()));
        }

        return listMealDTO;
    }

    @Override
    public MealDTO updateMeal(MealDTO mealDTO, long id) throws MealNotFoundException {


        Meal tempMeal = mealRepo.findById(id).orElse(null);
        if (mealDTO == null || tempMeal == null) {

            throw new MealNotFoundException("No mean found at: " + id + " -try right id");
        }

        tempMeal.setMealName(mealDTO.getMealName());

        return mealDTO;
    }

    @Override
    public void deleteMeal(String mealname) {

        Meal tempMeal = mealRepo.findByMealName(mealname);
        mealRepo.delete(tempMeal);
    }

    //search fore meal by its name, add korisnik1/2 to its list
    @Override
    public MealResponse mealKorisniks(String mealname, MealResponse mealResponse) {


        return null;
    }

    //search fore meal by its name, add korisnik1/2 to its list
    @Override
    public MealResponse mealKorisniksT1(String mealname) {

        Meal tempMeal = mealRepo.findByMealName(mealname);
        Korisnik tempKorisnik1 = new Korisnik("Mile", "Milic", "lemi@gmail.com");
        Korisnik tempKorisnik2 = new Korisnik("Darko", "Darkic", "reda@gmail.com");

        tempMeal.addKorisnikToMeal(tempKorisnik1);
        tempMeal.addKorisnikToMeal(tempKorisnik2);


       // KorisnikMeals korisnikMeals = new KorisnikMeals(tempMeal.getId(), tempKorisnik1.getId(), orderDate);

       // S save = mealRepo.save(korisnikMeals);

        MealResponse mealResponse = new MealResponse(tempMeal.getMealName(), tempMeal.getListKorisnik());

        return mealResponse;
    }

//    @Override
//    public List<Meal> getAll() {
//
//        List<Meal> listMeal = new ArrayList<>();
//        listMeal = (List)mealRepo.findAll();
//
//        return listMeal;
//    }
//
//    @Override
//    public Meal getOne(long id) {
//
//        Meal mealOne = mealRepo.findById(id).get();
//
//        return mealOne;
//    }
}
