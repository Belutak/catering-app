package com.cateringapp.service;

import com.cateringapp.exception.korisnik.KorisnikNotFoundException;
import com.cateringapp.model.Korisnik;
import com.cateringapp.rest.dto.korisnikDTO.KorisnikDTO;
import com.cateringapp.rest.dto.korisnikDTO.KorisnikResponse;

import java.util.List;


public interface KorisnikService {

    KorisnikDTO saveNew(KorisnikDTO korisnikDTO);

    List<KorisnikDTO> getAllNew();

    KorisnikDTO getByName(String firstname);

    void delete(String email);

    KorisnikDTO updateKorisnikNew(KorisnikDTO korisnikDTO, long id) throws KorisnikNotFoundException;
    public KorisnikResponse returnMealsT(String korisnikName);

//    public void create(Korisnik korisnik);
//    public Korisnik updateKorisnik(Korisnik korisnik, long id);
//    public Korisnik getOne(long id);
//    public List<Korisnik> getAll();
//    public void deleteWithId(long id);
}
