package com.cateringapp.service;

import com.cateringapp.exception.meal.MealNotFoundException;
import com.cateringapp.rest.dto.mealDTO.MealDTO;
import com.cateringapp.rest.dto.mealDTO.MealResponse;

import java.util.List;


public interface MealService {

    MealDTO saveNew(MealDTO mealDTO);

    MealDTO getByName(String mealName);

    List<MealDTO> getAll();

    MealDTO updateMeal(MealDTO mealDTO, long id) throws MealNotFoundException;

    void deleteMeal(String mealName);


    MealResponse mealKorisniks(String mealname, MealResponse mealResponse);
    public MealResponse mealKorisniksT1(String mealname);
//    public List<Meal> getAll();
//    public Meal getOne(long id);
}
