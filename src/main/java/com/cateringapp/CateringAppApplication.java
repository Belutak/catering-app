package com.cateringapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties
@EntityScan(basePackages = {"com.cateringapp.model"})
public class CateringAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(CateringAppApplication.class, args);
	}
}
