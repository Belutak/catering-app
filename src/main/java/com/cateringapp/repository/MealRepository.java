package com.cateringapp.repository;

import com.cateringapp.model.Meal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MealRepository extends CrudRepository<Meal, Long> {

    public Meal findByMealName(String mealName);
}
