package com.cateringapp.repository;

import com.cateringapp.model.Korisnik;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KorisnikRepository extends CrudRepository<Korisnik, Long> {

    public Korisnik findByFirstName(String firstName);
}
