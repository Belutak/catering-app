package com.cateringapp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Date;

@Entity
@Table(name = "korisnikmeals")
public class KorisnikMeals implements Serializable {

    @Id
    @Column(name = "korisnik_id")
    private long korisnik_id;

    @Id
    @Column(name = "meal_id")
    private long meal_id;

    @Column(name = "orderDate")
    private Date orderDate;


    public KorisnikMeals() {
    }

    public KorisnikMeals(long korisnik_id, long meal_id, Date orderDate) {
        this.korisnik_id = korisnik_id;
        this.meal_id = meal_id;
        this.orderDate = orderDate;
    }

    public long getKorisnik_id() {
        return korisnik_id;
    }

    public void setKorisnik_id(long korisnik_id) {
        this.korisnik_id = korisnik_id;
    }

    public long getMeal_id() {
        return meal_id;
    }

    public void setMeal_id(long meal_id) {
        this.meal_id = meal_id;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    @Override
    public String toString() {
        return "KorisnikMeals{" +
                "korisnik_id=" + korisnik_id +
                ", meal_id=" + meal_id +
                ", orderDate=" + orderDate +
                '}';
    }
}
