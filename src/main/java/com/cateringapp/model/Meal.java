package com.cateringapp.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "meal")
public class Meal {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "mealName")
    private String mealName;


    @ManyToMany(fetch = FetchType.EAGER,
            cascade = {CascadeType.DETACH, CascadeType.MERGE,
                    CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinTable(
            name = "korisnikmeals",
            joinColumns = @JoinColumn(name = "meal_id"),
            inverseJoinColumns = @JoinColumn(name = "korisnik_id")
    )
   // @JsonManagedReference
    @JsonBackReference
    private List<Korisnik> listKorisnik;

    public Meal() {
    }

    public Meal(String mealName) {
        this.mealName = mealName;
    }

    public Meal(long id, String mealName, List<Korisnik> listKorisnik) {
        this.id = id;
        this.mealName = mealName;
        this.listKorisnik = listKorisnik;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMealName() {
        return mealName;
    }

    public void setMealName(String mealName) {
        this.mealName = mealName;
    }

    public List<Korisnik> getListKorisnik() {
        return listKorisnik;
    }

    public void setListKorisnik(List<Korisnik> listKorisnik) {
        this.listKorisnik = listKorisnik;
    }

    public void addKorisnikToMeal(Korisnik korisnik) {

        if (listKorisnik == null) {

            listKorisnik = new ArrayList<>();
        }

        listKorisnik.add(korisnik );
    }

    @Override
    public String toString() {
        return "Meal{" +
                "id=" + id +
                ", mealName='" + mealName + '\'' +
                '}';
    }
}
