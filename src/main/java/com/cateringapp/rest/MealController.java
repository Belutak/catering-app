package com.cateringapp.rest;


import com.cateringapp.exception.meal.MealNotFoundException;
import com.cateringapp.rest.dto.mealDTO.MealDTO;
import com.cateringapp.rest.dto.mealDTO.MealResponse;
import com.cateringapp.service.MealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/meal")
public class MealController {

    @Autowired
    private MealService mealService;

    //create new meal
    @PostMapping("/createnew")
    public ResponseEntity createNew(@RequestBody MealDTO mealDTO) {

        return ResponseEntity.ok(mealService.saveNew(mealDTO));
    }

    //get all meal
    @GetMapping("/getall")
    public List<MealDTO> getAllMeal() {

        return mealService.getAll();
    }

    //get one meal by name search
    @GetMapping("/get/")
    public ResponseEntity getByName(@RequestParam(required = false, name = "mealname") String mealName) {

        return ResponseEntity.ok(mealService.getByName(mealName));
    }

    //update meal by id search
    @PutMapping("/update/{id}")
    public ResponseEntity updateMealkByIdNew(@RequestBody MealDTO mealDTO, @PathVariable long id) throws MealNotFoundException {

        return ResponseEntity.ok(mealService.updateMeal(mealDTO, id));
    }

    //delete meal with meal name search
    @DeleteMapping("/delete/")
    public void deleteMealCont(@RequestParam(required = false, name = "mealname") String mealname) {

        mealService.deleteMeal(mealname);
    }





    @PostMapping("/getk/")
    public MealResponse getKList(@RequestParam(required = false, name = "mealname") String mealname,
                               @RequestBody MealResponse mealResponse ) {

        return mealService.mealKorisniks(mealname, mealResponse);
    }

    @GetMapping("/getk1/")
    public MealResponse getKList(@RequestParam(required = false, name = "mealname") String mealname) {

        return mealService.mealKorisniksT1(mealname);
    }

    //@PostMapping

//    @GetMapping("/getallmeals")
//    public List<Meal> getAllMeal() {
//
//        List<Meal> listMeal = mealService.getAll();
//
//        return listMeal;
//    }
//
//    @GetMapping("get/{id}")
//    public Meal getOne(@PathVariable long id) {
//
//        Meal mealOne = mealService.getOne(id);
//
//        return mealOne;
//    }
}
