package com.cateringapp.rest;

import com.cateringapp.exception.korisnik.KorisnikNotFoundException;
import com.cateringapp.model.Korisnik;
import com.cateringapp.rest.dto.korisnikDTO.KorisnikDTO;
import com.cateringapp.rest.dto.korisnikDTO.KorisnikResponse;
import com.cateringapp.service.KorisnikService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/korisnik")
public class KorisnikController {

    @Autowired
    private KorisnikService korisnikService;


    //create new korisnik
    @PostMapping("/createnew")
    public ResponseEntity createNew(@RequestBody KorisnikDTO korisnikDTO) {

        return ResponseEntity.ok(korisnikService.saveNew(korisnikDTO));
    }

    //get all korisnik
    @GetMapping("/getallkorisnik")
    public List<KorisnikDTO> getAllKorisnikNew() {

        return korisnikService.getAllNew();
    }

    //get one korisnik by name search
    @GetMapping("/get/")
    public ResponseEntity getByName(@RequestParam(required = false, name = "firstname") String firstname) {

        return ResponseEntity.ok(korisnikService.getByName(firstname));
    }

    //update korisnik by id search
    @PutMapping("/updatenew/{id}")
    public ResponseEntity updateKorisnikByIdNew(@RequestBody KorisnikDTO korisnikDTO, @PathVariable long id) throws KorisnikNotFoundException {

        return ResponseEntity.ok(korisnikService.updateKorisnikNew(korisnikDTO, id));
    }

    //delete korisnik with name search
    @DeleteMapping("/delete/")
    public void deleteKorisnik(@RequestParam(required = false, name = "firstName") String firstName) {

        korisnikService.delete(firstName);
    }

    @PostMapping("/order/{idkorisnik}/{idmeal}/")
    public void makeOrder() {



    }

    @GetMapping("/getkm/")
    public KorisnikResponse GetMealsForKorisnik(@RequestParam(required = false, name = "firstname") String firstName) {


        return korisnikService.returnMealsT(firstName);

    }




//    @PostMapping("/create")
//    public Korisnik createKorisnik(@RequestBody Korisnik korisnik) {
//
//        korisnikService.create(korisnik);
//
//        return korisnik;
//    }
//
//    @GetMapping("get/{id}")
//    public Korisnik getOneKorisnik(@PathVariable long id) {
//
//        Korisnik korisnikOne = korisnikService.getOne(id);
//
//        return korisnikOne;
//    }
//
//    @GetMapping("/getallkorisnik")
//    public List<Korisnik> getAllKorisnik() {
//
//        List<Korisnik> listKorisnik = korisnikService.getAll();
//
//        return listKorisnik;
//    }
//
//    @PutMapping("/update/{id}")
//    public Korisnik updateKorisnikById(@RequestBody Korisnik korisnik, @PathVariable long id) {
//
//        return korisnikService.updateKorisnik(korisnik, id);
//    }
//
//
//    @DeleteMapping("delete/{id}")
//    public void deleteKorisnikById(@PathVariable long id) {
//
//        korisnikService.deleteWithId(id);
//    }


    //for testing
    @GetMapping("/hello")
    public String hello() {

        return "Hello";
    }
}
