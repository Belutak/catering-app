package com.cateringapp.rest.dto.korisnikDTO;

import com.cateringapp.model.Meal;

import java.util.List;

public class KorisnikResponse {

    private String firstName;

    private String lastName;

    private String email;


    private List<Meal> mealList;

    public KorisnikResponse(String firstName, String lastName, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public KorisnikResponse() {
    }

    public KorisnikResponse(String firstName, String lastName, String email, List<Meal> mealList) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.mealList = mealList;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Meal> getMealList() {
        return mealList;
    }

    public void setMealList(List<Meal> mealList) {
        this.mealList = mealList;
    }


}
