package com.cateringapp.rest.dto.mealDTO;

import com.cateringapp.model.Korisnik;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.util.ArrayList;
import java.util.List;

public class MealResponse {


    private String mealName;

    private List<Korisnik> listKorisnik;

    public MealResponse() {
    }

    public MealResponse(String mealName, List<Korisnik> listKorisnik) {
        this.mealName = mealName;
        this.listKorisnik = listKorisnik;
    }

    public String getMealName() {
        return mealName;
    }

    public void setMealName(String mealName) {
        this.mealName = mealName;
    }

    public List<Korisnik> getListKorisnik() {
        return listKorisnik;
    }

    public void setListKorisnik(List<Korisnik> listKorisnik) {
        this.listKorisnik = listKorisnik;
    }

    public void addKorisnikToMeal(Korisnik korisnik) {

        if (listKorisnik == null) {

            listKorisnik = new ArrayList<>();
        }

        listKorisnik.add(korisnik );
    }
}
