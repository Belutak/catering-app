package com.cateringapp.rest.dto.mealDTO;

public class MealDTO {

    private String mealName;

    public MealDTO() {
    }

    public MealDTO(String mealName) {
        this.mealName = mealName;
    }

    public String getMealName() {
        return mealName;
    }

    public void setMealName(String mealName) {
        this.mealName = mealName;
    }

    @Override
    public String toString() {
        return "MealDTO{" +
                "mealName='" + mealName + '\'' +
                '}';
    }
}
